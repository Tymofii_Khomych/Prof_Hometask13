﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace task2
{
    internal class Program
    {
        static void MyTask1()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.Write("1 ");
                Thread.Sleep(100);
            }
        }
        static void MyTask2()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.Write("2 ");
                Thread.Sleep(100);
            }
        }


        static void Main(string[] args)
        {
            ParallelOptions options = new ParallelOptions();
            Parallel.Invoke(options, MyTask1, MyTask2);
        }
    }
}
