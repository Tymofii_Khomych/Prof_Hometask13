﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10000000];
            Random rand = new Random();

            Parallel.For(0, arr.Length, (i) => arr[i] = rand.Next());

            ParallelQuery<int> odds = from i in arr.AsParallel()
                                      where i % 2 != 0
                                      select i;


            //foreach (int i in odds)
            //{
            //    Console.Write(i + " ");
            //}

            Console.WriteLine();

        }
    }
}
